﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Recargar colores al modificar la paleta
/// </summary>
[CustomEditor(typeof(PaletteSwapper))]
public class PaletteSwapperEditor : Editor{

	PaletteSwapper paletteSwapper;

	public override void OnInspectorGUI() {
		using (var check = new EditorGUI.ChangeCheckScope()) {
			base.OnInspectorGUI();
			if (check.changed) {
				paletteSwapper.UpdateColors();
			}
		}

	}

	public void OnEnable() {
		paletteSwapper = (PaletteSwapper)target;
	}
}
