﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour{

	[HideInInspector] public int handicap;
	public bool defending = false;
	public delegate void OnHandicapUpdate();
	public OnHandicapUpdate onHandicapUpdate;

	protected HandicapMovement hm;

	void Start(){
		handicap = 1;
    }

	/// <summary>
	/// Al subir el handicap lo actualiza
	/// </summary>
	/// <param name="extraHandicap">Handicap a añadir</param>
	public void AddHandicap(int extraHandicap) {
		handicap += extraHandicap;
		if(onHandicapUpdate != null)
			onHandicapUpdate.Invoke();

	}

	public void SetHM(HandicapMovement hm) {
		this.hm = hm;
	}
}
