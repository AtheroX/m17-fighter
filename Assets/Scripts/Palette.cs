﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "palette", menuName = "Palette", order = 1)]
public class Palette : ScriptableObject{
	public Color lightColor = Color.white;
	public Color medColor = Color.white;
	public Color darkColor = Color.white;
}
