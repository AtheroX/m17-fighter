﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HandicapMovement : MonoBehaviour{

	TextMeshProUGUI numbertxt;
	public Gradient g;
	static int id;
	public TextMeshProUGUI vidastxt;

	/// <summary>
	/// Actualiza los colores del handicap y pasa por delegado a la entidad que se actualice este handicap al ser golpeado
	/// </summary>
	/// <param name="e">Entidad de quien es el handicap</param>
	public void Set(Entity e) {
		//Entity[] entities = FindObjectsOfType<Entity>();
		//Entity e = entities[id];

		numbertxt = GetComponent<TextMeshProUGUI>();

		Palette colors = e.transform.GetChild(0).GetComponent<PaletteSwapper>().paleta;
		transform.parent.parent.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().color = new Color(colors.lightColor.r * 1.2f, colors.lightColor.g * 1.2f, colors.lightColor.b * 1.2f, 1);
		transform.parent.parent.GetComponent<Image>().color = new Color(colors.darkColor.r * 1.2f, colors.darkColor.g * 1.2f, colors.darkColor.b * 1.2f, 1);
		transform.parent.parent.GetChild(0).GetComponent<Image>().color = new Color(colors.lightColor.r * 2f, colors.lightColor.g * 2f, colors.lightColor.b * 2f, 1) + (Color.white / 10f);

		e.SetHM(this);
		e.onHandicapUpdate += () => {
			numbertxt.text = e.handicap + "";
			RectTransform child = transform.GetChild(0).GetComponent<RectTransform>();
			Vector3 pos = child.localPosition;
			child.localPosition =	numbertxt.text.Length == 3 ? new Vector3(100f, pos.y, pos.z) :
									numbertxt.text.Length == 2 ? new Vector3(80f, pos.y, pos.z) :
									new Vector3(55f, pos.y, pos.z);

			numbertxt.color = g.Evaluate(e.handicap / 100f);
			transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = g.Evaluate(e.handicap / 100f);
		};
		id++;
	}

	/// <summary>
	/// Cambia la cantidad de vidas
	/// </summary>
	/// <param name="i"></param>
	public void SetVida(int i) {
		vidastxt.SetText(i + "");
	}
}
