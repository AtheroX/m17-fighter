﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Utils {

	/// <summary>
	/// Devuelve lista de players ordenada de izquierda a derecha
	/// </summary>
	/// <param name="p">Lista de jugadores</param>
	/// <returns>Lista ordenada de izquierda a derecha</returns>
	public static List<Player> SortPlayers(this List<Player> p) {
		return p.OrderBy(x => Vector3.Distance(x.transform.position, -Vector3.right * 15)).ToList();
	}

	/// <summary>
	/// Devuelve lista de Handicaps ordenada de izquierda a derecha
	/// </summary>
	/// <param name="p">Lista de Handicaps</param>
	/// <returns>Lista ordenada de Handicaps</returns>
	public static List<HandicapMovement> SortHandicaps(this List<HandicapMovement> p) {
		return p.OrderBy(x => Vector3.Distance(x.transform.position, -Vector3.right * 15)).ToList();
	}
}
