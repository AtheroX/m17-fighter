﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity {

	[System.Serializable]
	public struct HitPoint {
		public Transform point;
		public float range;
	}

    public int playerID = 1;
	public int vidas = 3;

	[Header("Move")]
	public float moveSpeed = 2.5f;
	public float jumpForce = 5f;
	float fallMultiplier = 2.5f;
	float lowFallMultipier = 2f;
	public float runSpeed = 1;
	public bool canJump = false;
	public bool canMove = true;

	[Space]
	[Header("Collisions")]
	public LayerMask playersLayers;
	public HitPoint softPoint;
	public HitPoint medPoint;
	public HitPoint hardPoint;

	[Space]
	[Header("Attack")]
    public float basicComboCD = 0.3f;
	public GameObject bullet;
	public GameObject knife;
	bool getKeyDownAxisJump = false;
	bool getKeyDownAxisAttack = false;
	bool getKeyDownAxisSAttack = false;

	float shieldActualCD = 0;
	public float shieldCooldown = 3f;
	bool canUseShield = true;
	[HideInInspector] public GameObject shield;

	int actualCombo = 0; //Sistema de gestion de combos para no reiniciar los que no debería

    Rigidbody2D rb;
    Animator anim;


	void Start(){
		StartCoroutine(LateStart());
    }

	/// <summary>
	/// Como la id del player viene del GameManager un frame tarde, hago un lateStart, un start un frame más tarde
	/// </summary>
	/// <returns></returns>
	IEnumerator LateStart() {
		yield return new WaitForEndOfFrame();
		rb = GetComponent<Rigidbody2D>();
		anim = GetComponentInChildren<Animator>();
		canMove = true;
		actualCombo = 0;

		canUseShield = true;
		shield = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).gameObject;
	}

	void Update() {

		//Ignora en caso de no tener id aún
		if (playerID == 0)
			return;

		IsOutOfMap();

		CanJump();
		BetterJumpCheck();


		if (Input.GetAxisRaw("Jump" + playerID) != 0 && !getKeyDownAxisJump && !defending) {
			Jump();
			getKeyDownAxisJump = true;
		}

		if (Input.GetAxisRaw("Jump" + playerID) == 0)
			getKeyDownAxisJump = false;


		if (Input.GetAxis("Run" + playerID) != 0) {
			runSpeed = 1.33f;
			anim.speed = 1.33f;
		} else {
			runSpeed = 1;
			anim.speed = 1;
		}

		//AnimPlaying son las animaciones que permiten ejecutar otras, las full son si no está ejecutando otras que no sean las que permiten otras animaciones
		bool animPlaying = anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") || anim.GetCurrentAnimatorStateInfo(0).IsName("Running");
		bool fullAnimPlaying = !animPlaying 
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("lanzaCuchillo")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("puñal")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("puñal2")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("pium")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("pincho")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("punch")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("punch2");

		//Al usar el escudo va a defender, si lo suelta o este ha llegado a su maximo entra en CD
		if (Input.GetAxis("Defend" + playerID) != 0 && canUseShield && !fullAnimPlaying) 
			StartCoroutine(Defending());
		
		if (!canUseShield || (shieldActualCD != 0 && Input.GetAxis("Defend" + playerID) == 0)) 
			StartCoroutine(DefendCD());
		

		//Si hace ataque normal, puede hacerlo y no se está defendiendo ejecuta el ataque normal
		//si es una animación repetible no cuenta como tecla pulsada para poder repetir
		if (Input.GetAxis("Attack" + playerID) != 0 && !getKeyDownAxisAttack && animPlaying && !defending) {
			if(!anim.GetCurrentAnimatorStateInfo(0).IsName("puñal") || !anim.GetCurrentAnimatorStateInfo(0).IsName("puñal2"))
				getKeyDownAxisAttack = true;
			Attack(false);
		}

		if(Input.GetAxis("Attack" + playerID) == 0)
			getKeyDownAxisAttack = false;

		//Lo mismo que en el ataque normal pero sólo si no es para iniciar combo
        if (Input.GetAxis("SAttack" + playerID) != 0 && !getKeyDownAxisSAttack && animPlaying && !defending && anim.GetInteger("basicComboState") != 0) {
            getKeyDownAxisSAttack = true;
            Attack(true);
        }

        if (Input.GetAxis("SAttack" + playerID) == 0)
            getKeyDownAxisSAttack = false;
    }

	/// <summary>
	/// Comprueba si está fuera del mapa, si lo está lo respawnea
	/// </summary>
	private void IsOutOfMap() {
		Vector2 cam = GameManager.instance.cameraSize;
		if (transform.position.x < -cam.x*1.2f / 2 || transform.position.x > cam.x*1.2f / 2
			|| transform.position.y < -cam.y * 1.2f / 2 || transform.position.y > cam.y * 1.2f / 2) {

			StartCoroutine(Respawn());
		}
	}

	/// <summary>
	/// Quita una vida, lo resposiciona y a los 2 segundos puedes controlarlo de nuevo.
	/// Si está muerto llama a GameOver 
	/// </summary>
	/// <returns></returns>
	IEnumerator Respawn() {
		transform.position = new Vector2(0, 3);
		GetComponent<Rigidbody2D>().gravityScale = 0;
		vidas--;
		if (vidas <= 0) {
			GameManager.instance.GameOver(playerID);

			yield return null;
		}

		hm.SetVida(vidas);

		int ID = playerID;
		playerID = 0;
		handicap = 0;
		onHandicapUpdate();
		canMove = false;
		PaletteSwapper ps = transform.GetChild(0).GetComponent<PaletteSwapper>();

		StopMoving();
		for (float t = 0; t <= 1.0f; t += Time.deltaTime / 2) {
			ps.SetTransparency(t);
			yield return null;
		}
		GetComponent<Rigidbody2D>().gravityScale = 1;
		playerID = ID;
		canMove = true;
	}

	/// <summary>
	/// Control de movimiento
	/// </summary>
	void FixedUpdate(){
		if (playerID == 0)
			return;
        if (canMove && !defending) {
            float x = Input.GetAxis("Horizontal" + playerID);
            if (x != 0) {
                anim.SetBool("Running", true);
                if (x > 0)
                    transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), transform.localScale.y);
                else
                    transform.localScale = new Vector2(-Mathf.Abs(transform.localScale.x), transform.localScale.y);
            } else
                anim.SetBool("Running", false);
            Vector2 input = new Vector2(x * ( moveSpeed * 100 ) * runSpeed * Time.deltaTime, rb.velocity.y);

            if(x!= 0)
                rb.velocity = input;
        }
    }

	/// <summary>
	/// Bloquea el movimiento y deja de correr
	/// </summary>
    void StopMoving() {
        canMove = false;
        anim.SetBool("Running", false);
        rb.velocity = Vector2.zero;
    }

#region attacks
	/// <summary>
	/// Deja de moverse y ataca
	/// </summary>
	/// <param name="sattack">tipo de ataque, false=normal, true=especial</param>
	void Attack(bool sattack) {
        StopMoving();
        AttackCD(sattack);
	}

	/// <summary>
	/// para ataques anteriores y ejecuta el actual, por si aca
	/// </summary>
	/// <param name="sattack">tipo de ataque, false=normal, true=especial</param>
	void AttackCD(bool sattack) {
        int basicComboState = anim.GetInteger("basicComboState");
        StopCoroutine(BasicCombo(basicComboState, sattack));
        StartCoroutine(BasicCombo(basicComboState, sattack));
    }

	/// <summary>
	/// Arbol de combos
	///								| A pincho
	///				| A punch2 > 2	| B pium
	///								| A lanzaCuchillo
	/// A punch	> 1	| B puñal  > 3	| B puñal2 (REPETEABLE en > 4)
	/// 
	/// 
	/// 
	/// </summary>
	/// <param name="i">Estado del combo</param>
	/// <param name="sa">True = superattack</param>
	/// <returns></returns>
	IEnumerator BasicCombo(int i, bool sa) {
		/*Actual combo es un sistema para que al tener que resear el combo por tiempo únicamente resetee siempre y cuando
		 * siga estando en el mismo combo, que no me resetee otros combos porque sí */
		actualCombo++;
		int resetCombo = actualCombo;
		if (i == 0) {
			if (!sa) { //punch
				anim.SetInteger("basicComboState", 1);
				anim.SetTrigger("punch");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 1 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);
			}
		} else if (i == 1) {
			if (!sa) { // punch2
				anim.SetInteger("basicComboState", 2);
				anim.SetTrigger("punch2");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 2 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);
			} else { // puñal
				anim.SetInteger("basicComboState", 3);
				anim.SetTrigger("puñal");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 3 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);
			}

		} else if (i == 2) {
			if (!sa) { //pincho
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("pincho");
			} else { //pium
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("pium");
				GameObject b = Instantiate(bullet);
				b.transform.position = transform.position;
				b.GetComponent<Rigidbody2D>().velocity = (Vector2.right * 5 * transform.localScale.x) + (Vector2.up * 1.5f);
				b.GetComponent<Bullet>().myPlayer = this;
			}
		} else if (i == 3) {
			if (!sa) { //pincho
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("lanzaCuchillo");
				yield return new WaitForSeconds(.3f);
				GameObject b = Instantiate(knife);
				b.transform.position = transform.position;
				b.GetComponent<Rigidbody2D>().velocity = (Vector2.right * 7 * transform.localScale.x) + (Vector2.up * 1.2f);
				b.GetComponent<Bullet>().myPlayer = this;

			} else { //pium
				anim.SetInteger("basicComboState", 4);
				anim.SetTrigger("puñal2");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 4 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);

			}
		} else if (i == 4) {
			if (!sa) { //pincho
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("lanzaCuchillo");
				yield return new WaitForSeconds(.3f);
				GameObject b = Instantiate(knife);
				b.transform.position = transform.position;
				b.GetComponent<Rigidbody2D>().velocity = (Vector2.right * 7 * transform.localScale.x) + (Vector2.up * 1.2f);
				b.GetComponent<Bullet>().myPlayer = this;

			} else { //pium
				anim.SetInteger("basicComboState", 3);
				anim.SetTrigger("puñal");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 3 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);

			}
		}
	}

	/// <summary>
	/// Sistema que detecta la colisión con un enemigo y detecta con qué le ha dado
	/// El int significa con qué parte ha golpeado (1=soft,2=med,3=hard)
	/// </summary>
	/// <returns>Devuelve una tupla de tipo de ataque y a quién ha golpeado</returns>
	public (int, GameObject) OnAttack() {
		bool hit = false;
		GameObject hitEn = gameObject; //Obligatoriamente debe inicializarse

		/* Recorre la lista de golpes softs y si ha detectado un objeto se lo guarda y lo devuelve, sino
		 sigue a la lista de meds y después la de hards */
		Collider2D[] softHits = Physics2D.OverlapCircleAll(softPoint.point.position, softPoint.range, playersLayers);
		foreach (Collider2D hitted in softHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;

			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (1, hitEn);

		Collider2D[] medHits = Physics2D.OverlapCircleAll(medPoint.point.position, medPoint.range, playersLayers);
		foreach (Collider2D hitted in medHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;
			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (2, hitEn);

		Collider2D[] hardHits = Physics2D.OverlapCircleAll(hardPoint.point.position, hardPoint.range, playersLayers);
		foreach (Collider2D hitted in hardHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;
			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (3, hitEn);

		return (0, hitEn);
	}

	/// <summary>
	/// Coge al enemigo golpeado y le aplica el knock que debería en base a tu handicap y tipo de golpe 
	/// si es mayor de 100 hace un hitstop y alto knockback
	/// </summary>
	/// <param name="i">tipo de golpe</param>
	/// <param name="en">Objeto a empujar</param>
	public void HitEnemy(int i, GameObject en) {
		//print("HIT" + i + " :" + en.name);

		Entity eEn = en.GetComponent<Entity>();
		if (eEn == null)
			return;

		float threshold = 20f + (eEn.handicap / 100f);
		print(threshold);
		float randF = UnityEngine.Random.Range(1f, 101f);
		bool rand = randF< threshold;

		//gran knockback si >= de 100
		if (eEn.handicap >= 100 && rand && !eEn.defending) {
			StartCoroutine(FreezeGame(1f));
			Rigidbody2D enRb = en.GetComponent<Rigidbody2D>();
			Vector2 knock = en.transform.position - transform.position;
			knock.Normalize();
			float handicapVal = eEn.handicap/2f;
			enRb.AddForce(((knock * handicapVal) + Vector2.up) * ((i + 1) / 2) * 7, ForceMode2D.Impulse);
			print("HISSATSU");
		} else {
			Rigidbody2D enRb = en.GetComponent<Rigidbody2D>();
			Vector2 knock = en.transform.position - transform.position;
			knock.Normalize();
			float handicapVal = Mathf.Clamp(eEn.handicap * 0.03f, 1, eEn.handicap * 0.03f);
            if (eEn.defending) {
                knock /= 5f;
            }

			enRb.AddForce(((knock * handicapVal) + Vector2.up) * ((i + 1) / 2) * 7, ForceMode2D.Impulse);
		}
	}

	/// <summary>
	/// Congela el juego el tiempo especificado
	/// </summary>
	/// <param name="time">Tiempo a congelar</param>
	IEnumerator FreezeGame(float time) {
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime(time);
		Time.timeScale = 1;
	}


	/// <summary>
	/// Tiene un valor que significa el tiempo que puede usar el escudo, si este llega a 0 se bloquea y no puedes defender
	/// </summary>
	/// <returns></returns>
	IEnumerator Defending() {
		defending = true;
		if (shieldActualCD < shieldCooldown) {
			StopMoving();
			shieldActualCD += Time.deltaTime;
			float size = Mathf.Lerp(shieldCooldown/ shieldCooldown, 0, shieldActualCD/ shieldCooldown);
			shield.transform.localScale = new Vector2(.5f + .5f * size, .5f + .5f * size);
			shield.GetComponent<SpriteRenderer>().enabled = true;
		} else {
			defending = false;
			canUseShield = false;
			shield.transform.localScale = new Vector2(0,0);
			shield.GetComponent<SpriteRenderer>().enabled = false;
		}
		yield return null;
	}

	/// <summary>
	/// Disminuye el CD actual hasta llevarlo a 0
	/// </summary>
	/// <returns></returns>
	IEnumerator DefendCD() {
		defending = false;
		shield.GetComponent<SpriteRenderer>().enabled = false;
		if (shieldActualCD > 0)
			shieldActualCD -= Time.deltaTime;
		else
			canUseShield = true;

		yield return null;
	}
#endregion


#region jump

	/// <summary>
	/// Control de salto manteniendo el salto, simplemente aumenta la gravedad si no mantienes el salto
	/// </summary>
	private void BetterJumpCheck() {
		if (rb.velocity.y < 0)
			rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;

		if (rb.velocity.y > 0 && !Input.GetButton("Jump")) {
			rb.velocity += Vector2.up * Physics2D.gravity.y * (lowFallMultipier - 1) * Time.deltaTime;
		}
	}

	/// <summary>
	/// Hago raycast hacia abajo, si hay colision (sea suelo o jugador) puede saltar
	/// </summary>
    private void CanJump() {

		Vector2 raypos = new Vector2(transform.position.x + transform.lossyScale.x/4, transform.position.y - transform.lossyScale.y / 2);
		RaycastHit2D hit = Physics2D.Raycast(raypos, Vector2.down, 0.005f);
		Debug.DrawRay(raypos, Vector2.down * 0.005f);

		if (hit) {
            if (hit.collider != null && hit.collider != GetComponent<Collider2D>()) {
                canJump = true;
            }
        }
    }

	/// <summary>
	/// Cancela inercia vertical y aplica una fuerza ascendente
	/// </summary>
    private void Jump() {
        if (canJump) {
			rb.velocity = new Vector2(rb.velocity.x, 0);
			rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            canJump = false;
        }
    }
#endregion

	/// <summary>
	/// Dibuja las esferas de golpeo para facilidad al animar
	/// </summary>
	public void OnDrawGizmos() {
		if (hardPoint.point != null) {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(hardPoint.point.position, hardPoint.range);
		}
		if (medPoint.point != null) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(medPoint.point.position, medPoint.range);
		}
		if (softPoint.point != null) {

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(softPoint.point.position, softPoint.range);
		}
	}
}
