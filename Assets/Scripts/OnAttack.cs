﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAttack : StateMachineBehaviour{

	Player p;
	bool alreadyHit;
	
	
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		p = animator.gameObject.GetComponent<Player>();
		alreadyHit = false;
		
	}

	/// <summary>
	/// Mientras la animación se ejecuta intenta ver si el jugador ha golpeado llamando a p.OnAtack()
	/// En caso de haber dado va a hitEnemy pasandole el tipo de golpe y el enemigo golpeado
	/// También añade handicap adiente a si está defendiendose o no
	/// </summary>
	/// <param name="animator"></param>
	/// <param name="stateInfo"></param>
	/// <param name="layerIndex"></param>
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		int hit = -1;
		GameObject enemy = animator.gameObject;
		if (hit < 1 && !alreadyHit) {
			var thigs = p.OnAttack();
			hit = thigs.Item1;
			if (hit < 1) return;
			enemy = thigs.Item2;
			p.HitEnemy(hit, enemy);
			int extraHdc = Random.Range(1, 11);
			Entity e = enemy.GetComponent<Entity>();
			if (e.defending)
				extraHdc /= 5;
			e.AddHandicap(extraHdc);
			alreadyHit = true;
		}
	}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.GetComponent<Player>().canMove = true;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
