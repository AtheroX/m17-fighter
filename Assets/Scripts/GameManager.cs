﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour{

    public static GameManager instance;

    public Palette[] palette = new Palette[12];
    Palette[] usedPal = new Palette[2];
	public bool alreadyLoaded = false;

	public GameObject canvas;

	public Vector2 cameraSize;

    void Start(){
        if (instance == null) {
            instance = this;
			canvas = transform.GetChild(0).gameObject;
			canvas.SetActive(false);
            DontDestroyOnLoad(this.gameObject);
        } else
            Destroy(this.gameObject);
    }

	/// <summary>
	/// Se ejecuta al cargar una escena de juego y se espera un frame para que se genere todo
	/// tras eso lista a todos los jugadores y les pone su ID de izquierda a derecha, también lo hace con los Handicaps 
	/// y actualiza su paleta a la elegida 
	/// </summary>
	/// <returns></returns>
    IEnumerator ListPlayers() {
        if (SceneManager.GetActiveScene().buildIndex != 0 && !alreadyLoaded) {
			alreadyLoaded = true;
			yield return new WaitForEndOfFrame();

			Camera cam = Camera.main;
			cameraSize = cam.ScreenToWorldPoint(new Vector2(cam.pixelWidth * 1.5f, cam.pixelHeight * 1.5f));

			List<Player> p = new List<Player>(FindObjectsOfType<Player>());
            p = Utils.SortPlayers(p);

			List<HandicapMovement> hm = new List<HandicapMovement>(FindObjectsOfType<HandicapMovement>());
			hm = Utils.SortHandicaps(hm);

			for (int i = 0; i < p.Count; i++) {
                p[i].playerID = i + 1;
				if (p[i].transform.position.x > 0)
					p[i].transform.localScale = new Vector2(-p[i].transform.localScale.x, p[i].transform.localScale.y);
                p[i].gameObject.transform.GetChild(0).GetComponent<PaletteSwapper>().paleta = usedPal[i];
                p[i].gameObject.transform.GetChild(0).GetComponent<PaletteSwapper>().UpdateColors();
				p[i].shield.GetComponent<SpriteRenderer>().color = 
					new Color(0.75f - usedPal[i].lightColor.r, 0.75f - usedPal[i].lightColor.g, 0.75f - usedPal[i].lightColor.b, 1);
				hm[i].Set(p[i]);

			}
        } else {
            yield return new WaitForEndOfFrame();
			alreadyLoaded = false;
			StartCoroutine(ListPlayers());

        }
    }

	/// <summary>
	/// Guarda las paletas elegidas
	/// </summary>
	/// <param name="pal"></param>
    public void SetMaterials(int[] pal) {
        usedPal[0] = palette[pal[0]];
        usedPal[1] = palette[pal[1]];
        StartCoroutine(ListPlayers());
    }

	/// <summary>
	/// Quita el control a los jugadores y activa la ventana de gameover
	/// </summary>
	/// <param name="i">Ganador</param>
	public void GameOver(int i) {
		Player[] p = FindObjectsOfType<Player>();
		foreach (Player player in p) {
			player.enabled = false;
		}
		canvas.SetActive(true);
		canvas.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText("WINNER PLAYER"+i);
	}

	/// <summary>
	/// Vuelve al menu principal
	/// </summary>
	public void ReturnToMenu() {
		canvas.SetActive(false);
		SceneManager.LoadScene(0);
	}

	public void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(Vector2.zero, cameraSize);
	}
}
