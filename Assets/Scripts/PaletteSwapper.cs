﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteSwapper : MonoBehaviour {

	public Palette paleta;
	Material mat;
	SpriteRenderer sr;
	public bool onlyToMe = false;

	/// <summary>
	/// Actualiza el shader a la paleta actual
	/// </summary>
	public void UpdateColors() {
		if (paleta != null) {
			paleta.lightColor.a = 1;
			paleta.medColor.a = 1;
			paleta.darkColor.a = 1;
			mat = GetComponent<SpriteRenderer>().sharedMaterial;
			mat.SetColor("_ToLightColor", paleta.lightColor);
			mat.SetColor("_ToMedColor", paleta.medColor);
			mat.SetColor("_ToDarkColor", paleta.darkColor);
			if (!onlyToMe) {
				sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
				if (sr) {
					sr.sharedMaterial = mat;
				}

				sr = transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
				if (sr) {
					sr.sharedMaterial = mat;
				}
			}
		}
	}

	/// <summary>
	/// Actualiza la transparencia
	/// </summary>
	/// <param name="a">Transparencia</param>
	public void SetTransparency(float a) {
		if (paleta != null) {
			mat.SetFloat("_Alpha", a);
		}
	}
}