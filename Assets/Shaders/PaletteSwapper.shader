﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "ColorSwap"
{
    Properties
    {
        _MainTex("Sprite", 2D) = "white" {}
        _FromLightColor("_FromLightColor", Color) = (0,0,1,1)
        _ToLightColor("_ToLightColor ", Color) = (1,0,0,1)
        _FromMedColor("_FromMedColor", Color) = (0,0,1,1)
        _ToMedColor("_ToMedColor ", Color) = (1,0,0,1)
        _FromDarkColor("_FromDarkColor", Color) = (0,0,1,1)
        _ToDarkColor("_ToDarkColor ", Color) = (1,0,0,1)
		_Alpha("_Alpha", float) = 1
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque"
            "Queue" = "Transparent+1"
        }
		Cull off   
		Pass {
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMaterial AmbientAndDiffuse

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON

			sampler2D _MainTex;
			float4 _FromLightColor;
			float4 _ToLightColor;
			float4 _FromMedColor;
			float4 _ToMedColor;
			float4 _FromDarkColor;
			float4 _ToDarkColor;
			float _Alpha;
			
			struct Vertex
			{
				float4 vertex : POSITION;
				float2 uv_MainTex : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			struct Fragment
			{
				float4 vertex : POSITION;
				float2 uv_MainTex : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			Fragment vert(Vertex v)
			{
				Fragment o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv_MainTex = v.uv_MainTex;
				o.uv2 = v.uv2;

				return o;
			}

			float4 frag(Fragment IN) : COLOR
			{
				half4 c = tex2D(_MainTex, IN.uv_MainTex);
				
				if (c.r >= _FromLightColor.r - 0.005 && c.r <= _FromLightColor.r + 0.005
					&& c.g >= _FromLightColor.g - 0.005 && c.g <= _FromLightColor.g + 0.005
					&& c.b >= _FromLightColor.b - 0.005 && c.b <= _FromLightColor.b + 0.005){
					_ToLightColor.w = _Alpha;
					return _ToLightColor;
				}else if (c.r >= _FromMedColor.r - 0.005 && c.r <= _FromMedColor.r + 0.005
					&& c.g >= _FromMedColor.g - 0.005 && c.g <= _FromMedColor.g + 0.005
					&& c.b >= _FromMedColor.b - 0.005 && c.b <= _FromMedColor.b + 0.005){
					_ToMedColor.w = _Alpha;
					return _ToMedColor;
				}else if (c.r >= _FromDarkColor.r - 0.005 && c.r <= _FromDarkColor.r + 0.005
					&& c.g >= _FromDarkColor.g - 0.005 && c.g <= _FromDarkColor.g + 0.005
					&& c.b >= _FromDarkColor.b - 0.005 && c.b <= _FromDarkColor.b + 0.005){
					_ToDarkColor.w = _Alpha;
					return _ToDarkColor;
				}
				//c.w = _Alpha;
				return c;
			}
            ENDCG
        }
    }
}