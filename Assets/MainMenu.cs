﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu: MonoBehaviour {

    int playerToSelect;
    public GameObject[] players = new GameObject[2];
    public Material[] mats = new Material[12];
    public GameObject[] menus = new GameObject[3];
	public Transform[] buttonsFolder = new Transform[2];

	int levelToLoad = 0;

    public GameObject gotoSelectLevel, playLevel;

    int[] selPalettes = new int[2];

    void Start() {
        playerToSelect = 1;

		selPalettes[0] = -1;
		selPalettes[1] = -1;

        gotoSelectLevel.SetActive(false);
		playLevel.SetActive(false);

		//Coge todos los menus (main menu, selec. color y selec. nivel) y solo activa el primero
        foreach (GameObject m in menus) {
            m.SetActive(false);
        }
        menus[0].SetActive(true);
		
	}

	/// <summary>
	/// Selecciona un color de la lista, si el color es 12 selecciona uno aleatorio.
	/// También tiene un valor que es para quien está seleccionando y así poner verde o azul el selector (cian si ambos tienen el mismo)
	/// Después de seleccionar el segundo color se ve el play
	/// </summary>
	/// <param name="i"></param>
    public void selectCharacter(int i) {
        playerToSelect = playerToSelect == 1 ? 0 : 1;
        if (i == 12) {
            int rand = Random.Range(0, mats.Length);
            players[playerToSelect].GetComponent<SpriteRenderer>().sharedMaterial = mats[rand];
            selPalettes[playerToSelect] = rand;
			

		} else {
            players[playerToSelect].GetComponent<SpriteRenderer>().sharedMaterial = mats[i];
            selPalettes[playerToSelect] = i;
        }

		for (int j = 0; j < buttonsFolder[0].childCount; j++) {
			if (j == selPalettes[0]) {
				if(selPalettes[0] == selPalettes[1])
					buttonsFolder[0].GetChild(j).GetComponent<Image>().color = Color.cyan;
				else
					buttonsFolder[0].GetChild(j).GetComponent<Image>().color = Color.green;
			} else if (j == selPalettes[1])
				buttonsFolder[0].GetChild(j).GetComponent<Image>().color = Color.blue;
			else
				buttonsFolder[0].GetChild(j).GetComponent<Image>().color = Color.white;
		}

		if (playerToSelect == 1)
            DisplayPlay();
    }

    public Material[] getMaterials() {
        return mats;
    }

	/// <summary>
	/// Muestra el boton de play
	/// </summary>
    void DisplayPlay() {
        gotoSelectLevel.SetActive(true);
    }


	/// <summary>
	/// Guarda el nivel a jugar
	/// </summary>
	/// <param name="i">nivel a jugar</param>
	public void SelectLevel(int i) {
		levelToLoad = i;
		playLevel.SetActive(true);

	}

	/// <summary>
	/// Carga el nivel seleccionado
	/// </summary>
    public void PlayLevel() {
        GameManager.instance.SetMaterials(selPalettes);
        SceneManager.LoadScene(levelToLoad);
    }

    public void ExitGame() {
        Application.Quit();
    }

	/// <summary>
	/// Cambio de menus, desactiva todos
	/// </summary>
	/// <param name="i">activa el menu en posición i</param>
    public void LoadMenu(int i) {
        foreach (GameObject m in menus) {
            m.SetActive(false);
        }
		if(i<menus.Length)
			menus[i].SetActive(true);
    }
}
