﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	[System.Serializable]
	public struct HitPoint {
#if UNITY_ENGINE
		public string Name;
#endif
		public Transform point;
		public float range;
	}

	[Header("Move")]
	public float moveSpeed = 2.5f;
	public float jumpForce = 5f;
	float fallMultiplier = 2.5f;
	float lowFallMultipier = 2f;
	public float runSpeed = 1;
	public bool canJump = false;

	[Space]
	[Header("Collisions")]
	public LayerMask playersLayers;
	public HitPoint softPoint;
	public HitPoint medPoint;
	public HitPoint hardPoint;

	[Space]
	[Header("Attack")]
	//[HideInInspector] public int basicComboState = 1;
	public float basicComboCD = 1f;
	bool getKeyDownAxisAttack = false;


    SpriteRenderer sr;
    Rigidbody2D rb;
    Animator anim;
    Camera cam;

    void Start(){
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponentInChildren<SpriteRenderer>();
        anim = GetComponentInChildren<Animator>();
        cam = FindObjectOfType<Camera>();
    }


    void Update() {
		BetterJumpCheck();
		CanJump();

        if (Input.GetAxis("Jump") != 0) {
			Jump();
        }
		if (Input.GetAxis("Vertical") >= Mathf.Abs(Input.GetAxis("Horizontal")) && Input.GetAxis("Vertical") != 0) {
				Jump();
		}

		if (Input.GetAxis("Run") != 0) {
			runSpeed = 1.33f;
			anim.speed = 1.33f;
		} else {
			runSpeed = 1;
			anim.speed = 1;
		}

		if (Input.GetAxis("Attack") != 0 && !getKeyDownAxisAttack) {
			getKeyDownAxisAttack = true;
			Attack();
		}
		if(Input.GetAxis("Attack") == 0)
			getKeyDownAxisAttack = false;

		if (Input.GetAxis("SAttack") != 0)
			SAttack();
    }

	void FixedUpdate(){
#region mover
        Vector2 currentPos = rb.position;
        float x = Input.GetAxis("Horizontal");
        if (x != 0) {
            anim.SetBool("Running", true);
            if (x > 0) 
                transform.localScale = new Vector3(1, 1, 1);
			else
				transform.localScale = new Vector3(-1, 1, 1);
		} else
            anim.SetBool("Running", false);
        Vector2 input = new Vector2(x * (moveSpeed*100)* runSpeed * Time.deltaTime, rb.velocity.y);

        rb.velocity = input;
#endregion

    }

#region attacks
	void Attack() {
		AnimatorStateInfo an = anim.GetCurrentAnimatorStateInfo(0);
		if (an.IsName("attack1"))//&&otras animaciones de ataque
			return;

		int basicComboState = anim.GetInteger("basicComboState");
		StopCoroutine(BasicCombo(basicComboState));
		StartCoroutine(BasicCombo(basicComboState));
		
	}

	IEnumerator BasicCombo(int i) {
		//print(i);
		if (i == 0) {
			anim.SetInteger("basicComboState",1);
			anim.SetTrigger("punch");
			yield return new WaitForSeconds(basicComboCD);
			if(anim.GetInteger("basicComboState") == 1)
				anim.SetInteger("basicComboState", 0);
		} else if (i == 1) {
			anim.SetInteger("basicComboState", 0);
			anim.SetTrigger("punch2");
		}

	}

	void SAttack() {
		AnimatorStateInfo an = anim.GetCurrentAnimatorStateInfo(0);
		if (an.IsName("attack1") || an.IsName("punch") || an.IsName("punch2"))//&&otras animaciones de ataque
			return;

		anim.SetTrigger("Attack1");
	}

	public (int, GameObject) OnAttack() {
		bool hit = false;
		GameObject hitEn = gameObject; //Obligatoriamente debe inicializarse

		Collider2D[] softHits = Physics2D.OverlapCircleAll(softPoint.point.position, softPoint.range, playersLayers);
		foreach (Collider2D hitted in softHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;

			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (1, hitEn);

		Collider2D[] medHits = Physics2D.OverlapCircleAll(medPoint.point.position, medPoint.range, playersLayers);
		foreach (Collider2D hitted in medHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;
			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (2, hitEn);

		Collider2D[] hardHits = Physics2D.OverlapCircleAll(hardPoint.point.position, hardPoint.range, playersLayers);
		foreach (Collider2D hitted in hardHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;
			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (3, hitEn);

		return (0, hitEn);
	}

	public void HitEnemy(int i, GameObject en) {
		print("HIT" + i + " :" + en.name);
		Rigidbody2D enRb = en.GetComponent<Rigidbody2D>();
		Vector2 knock = en.transform.position - transform.position;
		enRb.AddForce((knock + Vector2.up) * ((i+1)/2) * 7, ForceMode2D.Impulse);
	}
#endregion


#region jump
	private void BetterJumpCheck() {
		if (rb.velocity.y < 0)
			rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;

		if (rb.velocity.y > 0 && !Input.GetButton("Jump")) {
			rb.velocity += Vector2.up * Physics2D.gravity.y * (lowFallMultipier - 1) * Time.deltaTime;
		}
	}

    private void CanJump() {

        Vector2 raypos = new Vector2(transform.position.x, transform.position.y - transform.lossyScale.y/2);
        RaycastHit2D hit = Physics2D.Raycast(raypos, Vector2.down, 0.2f);
        Debug.DrawRay(raypos, Vector2.down * 0.2f);

        if (hit) {
            if (hit.collider != null && hit.collider != GetComponent<Collider2D>()) {
                canJump = true;
            }
        }
    }
    private void Jump() {
        if (canJump) {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            canJump = false;
        }
    }
#endregion

	public void OnDrawGizmos() {
		if (hardPoint.point != null) {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(hardPoint.point.position, hardPoint.range);
		}
		if (medPoint.point != null) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(medPoint.point.position, medPoint.range);
		}
		if (softPoint.point != null) {

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(softPoint.point.position, softPoint.range);
		}
	}
}
