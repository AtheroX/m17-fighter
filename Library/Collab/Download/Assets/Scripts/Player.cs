﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity {

	[System.Serializable]
	public struct HitPoint {
		public Transform point;
		public float range;
	}

    public int playerID = 1;

	[Header("Move")]
	public float moveSpeed = 2.5f;
	public float jumpForce = 5f;
	float fallMultiplier = 2.5f;
	float lowFallMultipier = 2f;
	public float runSpeed = 1;
	public bool canJump = false;
	public bool canMove = true;

	[Space]
	[Header("Collisions")]
	public LayerMask playersLayers;
	public HitPoint softPoint;
	public HitPoint medPoint;
	public HitPoint hardPoint;

	[Space]
	[Header("Attack")]
    //[HideInInspector] public int basicComboState = 1;
    public float basicComboCD = 0.3f;
	public GameObject bullet;
	public GameObject knife;
	bool getKeyDownAxisJump = false;
	bool getKeyDownAxisAttack = false;
	bool getKeyDownAxisSAttack = false;
	bool getKeyDownAxisDefend = false;

	float shieldActualCD = 0;
	public float shieldCooldown = 3f;
	bool canUseShield = true;
	[HideInInspector] public GameObject shield;

	int actualCombo = 0; //Sistema de gestion de combos para no reiniciar los que no debería

    Rigidbody2D rb;
    Animator anim;


    void Start(){
		StartCoroutine(LateStart());
    }

	IEnumerator LateStart() {
		yield return new WaitForEndOfFrame();
		rb = GetComponent<Rigidbody2D>();
		anim = GetComponentInChildren<Animator>();
		canMove = true;
		actualCombo = 0;

		canUseShield = true;
		shield = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).gameObject;
	}

	void Update() {

		if (playerID == 0)
			return;

		BetterJumpCheck();
		CanJump();

		if (Input.GetAxisRaw("Jump" + playerID) != 0 && !getKeyDownAxisJump && !defending) {
			Jump();
			getKeyDownAxisJump = true;
		}
		if (Input.GetAxisRaw("Jump" + playerID) == 0)
			getKeyDownAxisJump = false;


		if (Input.GetAxis("Run" + playerID) != 0) {
			runSpeed = 1.33f;
			anim.speed = 1.33f;
		} else {
			runSpeed = 1;
			anim.speed = 1;
		}


		bool animPlaying = anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") || anim.GetCurrentAnimatorStateInfo(0).IsName("Running");
		bool fullAnimPlaying = !animPlaying 
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("lanzaCuchillo")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("puñal")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("puñal2")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("pium")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("pincho")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("punch")
			|| anim.GetCurrentAnimatorStateInfo(0).IsName("punch2");

		if (Input.GetAxis("Defend" + playerID) != 0 && canUseShield && !fullAnimPlaying) {
			StartCoroutine(Defending());
			getKeyDownAxisDefend = true;
		} 
		if (!canUseShield || (shieldActualCD != 0 && Input.GetAxis("Defend" + playerID) == 0)) {
			StartCoroutine(DefendCD());
			getKeyDownAxisDefend = false;
		}


		if (Input.GetAxis("Attack" + playerID) != 0 && !getKeyDownAxisAttack && animPlaying && !defending) {
			if(!anim.GetCurrentAnimatorStateInfo(0).IsName("puñal") || !anim.GetCurrentAnimatorStateInfo(0).IsName("puñal2"))
				getKeyDownAxisAttack = true;
			Attack(false);
		}
		if(Input.GetAxis("Attack" + playerID) == 0)
			getKeyDownAxisAttack = false;

        if (Input.GetAxis("SAttack" + playerID) != 0 && !getKeyDownAxisSAttack && animPlaying && !defending && anim.GetInteger("basicComboState") != 0) {
            getKeyDownAxisSAttack = true;
            Attack(true);
        }

        if (Input.GetAxis("SAttack" + playerID) == 0)
            getKeyDownAxisSAttack = false;
    }

	void FixedUpdate(){
		#region mover
		if (playerID == 0)
			return;
        if (canMove && !defending) {
            float x = Input.GetAxis("Horizontal" + playerID);
            if (x != 0) {
                anim.SetBool("Running", true);
                if (x > 0)
                    transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), transform.localScale.y);
                else
                    transform.localScale = new Vector2(-Mathf.Abs(transform.localScale.x), transform.localScale.y);
            } else
                anim.SetBool("Running", false);
            Vector2 input = new Vector2(x * ( moveSpeed * 100 ) * runSpeed * Time.deltaTime, rb.velocity.y);

            if(x!= 0)
                rb.velocity = input;
        }
#endregion
    }

    void StopMoving() {
        canMove = false;
        anim.SetBool("Running", false);
        rb.velocity = Vector2.zero;
    }

#region attacks
	void Attack(bool sattack) {
        StopMoving();
        StartCoroutine(AttackCD(sattack));
	}

    IEnumerator AttackCD(bool sattack) {
        int basicComboState = anim.GetInteger("basicComboState");
        StopCoroutine(BasicCombo(basicComboState, sattack));
        StartCoroutine(BasicCombo(basicComboState, sattack));
		AnimatorStateInfo an = anim.GetCurrentAnimatorStateInfo(0);
		yield return new WaitForSeconds(an.length);
    }

	/// <summary>
	/// 
	///								| J pincho
	///				| J punch2 > 2	| K pium
	///								| J lanzaCuchillo
	/// J punch	> 1	| K puñal  > 3	| K puñal2 (REPETEABLE en > 4)
	/// k ?????
	/// 
	/// 
	/// </summary>
	/// <param name="i">Estado del combo</param>
	/// <param name="sa">True = superattack</param>
	/// <returns></returns>
	IEnumerator BasicCombo(int i, bool sa) {
		actualCombo++;
		int resetCombo = actualCombo;
		//print(i);
		if (i == 0) {
			if (!sa) { //punch
				anim.SetInteger("basicComboState", 1);
				anim.SetTrigger("punch");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 1 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);
			}
			//else { // ????
			//	// TODO ------------------------------------
			//	anim.SetInteger("basicComboState", 1);
			//	anim.SetTrigger("punch");
			//	yield return new WaitForSeconds(basicComboCD);
			//	if (anim.GetInteger("basicComboState") == 1 && actualCombo == resetCombo)
			//		anim.SetInteger("basicComboState", 0);
			//}
		} else if (i == 1) {
			if (!sa) { // punch2
				anim.SetInteger("basicComboState", 2);
				anim.SetTrigger("punch2");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 2 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);
			} else { // puñal
				anim.SetInteger("basicComboState", 3);
				anim.SetTrigger("puñal");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 3 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);
			}

		} else if (i == 2) {
			if (!sa) { //pincho
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("pincho");
			} else { //pium
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("pium");
				GameObject b = Instantiate(bullet);
				b.transform.position = transform.position;
				b.GetComponent<Rigidbody2D>().velocity = (Vector2.right * 5 * transform.localScale.x) + (Vector2.up * 1.5f);
				b.GetComponent<Bullet>().myPlayer = this;
			}
		} else if (i == 3) {
			if (!sa) { //pincho
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("lanzaCuchillo");
				yield return new WaitForSeconds(.3f);
				GameObject b = Instantiate(knife);
				b.transform.position = transform.position;
				b.GetComponent<Rigidbody2D>().velocity = (Vector2.right * 7 * transform.localScale.x) + (Vector2.up * 1.2f);
				b.GetComponent<Bullet>().myPlayer = this;

			} else { //pium
				anim.SetInteger("basicComboState", 4);
				anim.SetTrigger("puñal2");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 4 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);

			}
		} else if (i == 4) {
			if (!sa) { //pincho
				anim.SetInteger("basicComboState", 0);
				anim.SetTrigger("lanzaCuchillo");
				yield return new WaitForSeconds(.3f);
				GameObject b = Instantiate(knife);
				b.transform.position = transform.position;
				b.GetComponent<Rigidbody2D>().velocity = (Vector2.right * 7 * transform.localScale.x) + (Vector2.up * 1.2f);
				b.GetComponent<Bullet>().myPlayer = this;

			} else { //pium
				anim.SetInteger("basicComboState", 3);
				anim.SetTrigger("puñal");
				yield return new WaitForSeconds(basicComboCD);
				if (anim.GetInteger("basicComboState") == 3 && actualCombo == resetCombo)
					anim.SetInteger("basicComboState", 0);

			}
		}
	}

	public (int, GameObject) OnAttack() {
		bool hit = false;
		GameObject hitEn = gameObject; //Obligatoriamente debe inicializarse

		Collider2D[] softHits = Physics2D.OverlapCircleAll(softPoint.point.position, softPoint.range, playersLayers);
		foreach (Collider2D hitted in softHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;

			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (1, hitEn);

		Collider2D[] medHits = Physics2D.OverlapCircleAll(medPoint.point.position, medPoint.range, playersLayers);
		foreach (Collider2D hitted in medHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;
			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (2, hitEn);

		Collider2D[] hardHits = Physics2D.OverlapCircleAll(hardPoint.point.position, hardPoint.range, playersLayers);
		foreach (Collider2D hitted in hardHits) {
			if (hitted.gameObject.Equals(this.gameObject)) continue;
			hit = true;
			hitEn = hitted.gameObject;
		}
		if (hit) return (3, hitEn);

		return (0, hitEn);
	}

	public void HitEnemy(int i, GameObject en) {
		//print("HIT" + i + " :" + en.name);

		Entity eEn = en.GetComponent<Entity>();
		float threshold = 20f + (eEn.handicap / 100f);
		print(threshold);
		float randF = UnityEngine.Random.Range(1f, 101f);
		bool rand = randF< threshold;

		//gran knockback si >= de 100
		if (eEn.handicap >= 100 && rand && !eEn.defending) {
			StartCoroutine(FreezeGame(1f));
			Rigidbody2D enRb = en.GetComponent<Rigidbody2D>();
			Vector2 knock = en.transform.position - transform.position;
			knock.Normalize();
			float handicapVal = eEn.handicap/2f;
			enRb.AddForce(((knock * handicapVal) + Vector2.up) * ((i + 1) / 2) * 7, ForceMode2D.Impulse);
			print("HISSATSU");
		} else {
			Rigidbody2D enRb = en.GetComponent<Rigidbody2D>();
			Vector2 knock = en.transform.position - transform.position;
			knock.Normalize();
			float handicapVal = Mathf.Clamp(eEn.handicap * 0.03f, 1, eEn.handicap * 0.03f);
            if (eEn.defending) {
                knock /= 5f;
            }

			enRb.AddForce(((knock * handicapVal) + Vector2.up) * ((i + 1) / 2) * 7, ForceMode2D.Impulse);
		}
	}

	IEnumerator FreezeGame(float time) {
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime(time);
		Time.timeScale = 1;
	}

	IEnumerator Defending() {
		defending = true;
		if (shieldActualCD < shieldCooldown) {
			shieldActualCD += Time.deltaTime;
			float size = Mathf.Lerp(shieldCooldown/ shieldCooldown, 0, shieldActualCD/ shieldCooldown);
			shield.transform.localScale = new Vector2(.5f + .5f * size, .5f + .5f * size);
			shield.GetComponent<SpriteRenderer>().enabled = true;
		} else {
			defending = false;
			canUseShield = false;
			shield.transform.localScale = new Vector2(0,0);
			shield.GetComponent<SpriteRenderer>().enabled = false;
		}
		yield return null;
	}

	IEnumerator DefendCD() {
		defending = false;
		shield.GetComponent<SpriteRenderer>().enabled = false;
		if (shieldActualCD > 0)
			shieldActualCD -= Time.deltaTime;
		else
			canUseShield = true;

		yield return null;
	}
#endregion


#region jump
	private void BetterJumpCheck() {
		if (rb.velocity.y < 0)
			rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;

		if (rb.velocity.y > 0 && !Input.GetButton("Jump")) {
			rb.velocity += Vector2.up * Physics2D.gravity.y * (lowFallMultipier - 1) * Time.deltaTime;
		}
	}

    private void CanJump() {

		Vector2 raypos = new Vector2(transform.position.x + transform.lossyScale.x/4, transform.position.y - transform.lossyScale.y / 2);
		Vector2 raypos2 = new Vector2(transform.position.x - transform.lossyScale.x/4, transform.position.y - transform.lossyScale.y / 2);
		RaycastHit2D hit = Physics2D.Raycast(raypos, Vector2.down, 0.005f);
		Debug.DrawRay(raypos, Vector2.down * 0.005f);
		Debug.DrawRay(raypos2, Vector2.down * 0.005f);

		if (hit) {
            if (hit.collider != null && hit.collider != GetComponent<Collider2D>()) {
                canJump = true;
            }
        }
    }
    private void Jump() {
        if (canJump) {
			rb.AddForce(Vector2.zero);
			rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            canJump = false;
        }
    }
#endregion

	public void OnDrawGizmos() {
		if (hardPoint.point != null) {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(hardPoint.point.position, hardPoint.range);
		}
		if (medPoint.point != null) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(medPoint.point.position, medPoint.range);
		}
		if (softPoint.point != null) {

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(softPoint.point.position, softPoint.range);
		}
	}
}
