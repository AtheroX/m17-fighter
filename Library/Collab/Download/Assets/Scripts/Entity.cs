﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour{

	[HideInInspector] public int handicap;
	public bool defending = false;
	public delegate void OnHandicapUpdate();
	public OnHandicapUpdate onHandicapUpdate;


    void Start(){
		handicap = 1;
        InvokeRepeating("SlowUpdate",0.2f,0.2f);
    }

    void SlowUpdate() {
        if (transform.position.y <= -6f) {
            if (GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().MovePosition(Vector2.zero);
            else
                transform.position = Vector2.zero;
        }
    }

	internal void AddHandicap(int extraHandicap) {
		handicap += extraHandicap;
		if(onHandicapUpdate != null)
			onHandicapUpdate.Invoke();

	}
}
