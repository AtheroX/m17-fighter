Funcionamento
Un fighter del Among us, el mejor juego del mundo.

Controls

Jugador1:
- WASD para movimiento
- LeftShift para correr
- G ataque flojo
- H ataque para combos
- J escudo

Jugador2:
- Flechitas para movimiento
- RightShift para correr
- , ataque flojo
- . ataque para combos
- / escudo

Risketos Mínims
- Hitbox i Hurtbox, la hitbox sale únicamente por la animación haciendo referencia los structs de HitPoint
- Barra de vida o similar (que es tindrà actualitzada per delegats)
- Les animacions han de moure les hitboxes i hurtboxes corresponents
- Les colisions tenen que estar correctament implementades, amb opció de bloqueig. (Bloqueas un 80%)
- Els impactes han de tenir reaccions físiques (haura de sortir rebutjat, o recular, o caure)
- Possibles combos, controlats per temps amb corrutines
- Projectils (amb físiques complexes, que no vagin en linea recta)
- Efectes de particles (Sólo en el menú principal)

Risketos Adicionales
- Uso structs
- Effector de agua
- Uso de el TimeScale para hacer un HitStop
- Shader
- ScripteableObjects
- Uso de Corrutinas complejas
